import Vue from 'vue'
import Index from '../../static/Index'
import {getCookie,setCookie} from '@/utils/store'
import Router from 'vue-router'
Vue.use(Router)

const router = new Router({
  routes: [
    {path: '/', name: 'Index',meta:{title:'登陆',requireAuth: false}, component: Index}

  ]
})
router.beforeEach((to, from, next) => {//beforeEach是router的钩子函数，在进入路由前执行
  if (to.meta.title) {//判断是否有标题
    document.title = to.meta.title
  }
  if (to.meta.requireAuth) {  // 判断该路由是否需要登录权限
    if (getCookie("sid") != null) {  // 通过vuex state获取当前的token是否存在
      next();
    }
    else {
      next({
        path: '/',
        query: {redirect: to.fullPath}  // 将跳转的路由path作为参数，登录成功后跳转到该路由
      })
    }
  }
  else {
    next();
  }
})
export default router;
