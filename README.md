# web

> vue+element初始框架组装

## Build Setup

``` bash
# 安装依赖
cnpm install

# 运行 localhost:8080
cnpm run dev

# 打包 默认prod
npm run build

# build for production and view the bundle analyzer report
npm run build --report

# run unit tests
npm run unit

# run all tests
npm test
```

# 集成axios 和 自动加上token
```
import axios from 'axios';
import {getCookie} from '@/utils/store'
import {mergeJsonObject} from '@/utils/jsonArr'

let http = axios.create({
  baseURL: process.env.BASE_API, // api的base_url
  timeout: 5000,
  withCredentials: true,
  headers: {
    'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8'
  },
  transformRequest: [function (data) {
    let newData = '';
    for (let k in data) {
      if (data.hasOwnProperty(k) === true) {
        newData += encodeURIComponent(k) + '=' + encodeURIComponent(data[k]) + '&';
      }
    }
    return newData;
  }]
});

function apiAxios(method, url, params, response) {
  if(getCookie("sid") != null){  //默认加上token
    params = mergeJsonObject(params,{"sid":getCookie("sid")})
    console.log(params)
  }
  http({
    method: method,
    url: url,
    data: method === 'POST' || method === 'PUT' ? params : null,
    params: method === 'GET' || method === 'DELETE' ? params : null,
  }).then(function (res) {
    response(res);
  }).catch(function (err) {
    response(err);
  })
}
export default {
  get: function (url, params, response) {
    return apiAxios('GET', url, params, response)
  },
  post: function (url, params, response) {
    return apiAxios('POST', url, params, response)
  },
  put: function (url, params, response) {
    return apiAxios('PUT', url, params, response)
  },
  delete: function (url, params, response) {
    return apiAxios('DELETE', url, params, response)
  }
}

```
# 页面token控制
```
const router = new Router({
  routes: [
    {path: '/', name: 'Index',meta:{title:'登陆',requireAuth: false}, component: Index} //requireAuth 是否需要token才能访问
  ]
})
router.beforeEach((to, from, next) => {//beforeEach是router的钩子函数，在进入路由前执行
  if (to.meta.title) {//判断是否有标题
    document.title = to.meta.title
  }
  if (to.meta.requireAuth) {  // 判断该路由是否需要登录权限
    if (getCookie("sid") != null) {  // 通过vuex state获取当前的token是否存在
      next();
    }
    else {
      next({
        path: '/',
        query: {redirect: to.fullPath}  // 将跳转的路由path作为参数，登录成功后跳转到该路由
      })
    }
  }
  else {
    next();
  }
})
```

# 网络请求
```

this.$api.post('user/login.py', {
          "login_name": this.login_name,
          "login_pwd": this.login_pwd
        }, response => {
          console.log(response.data.code )
          if (response.data.code === 200) {
            this.loading = false;
            console.log(response.data.data)
            setCookie("sid",response.data.data)
            this.$router.push({path: '/'});
          } else {
            console.log(response.data.message);
            this.loading = false;
            this.$message.error(response.data.message);
          }
          this.isDisable=false
        })
```

* 供新手学习 和 一般项目的使用
